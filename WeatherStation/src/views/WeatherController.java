package views;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;
import modele.*;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class WeatherController extends FXMLWindow {
    TableColumn<Sensor, String> nameCol = new TableColumn<>("Name");
    TableColumn<Sensor, Double> tempCol = new TableColumn<>("Temperature");

    @FXML
    TableView tableview;
    List<Sensor> sensorList;
    TreeItem<Sensor> selectedSensor = null;
    @FXML
    Spinner<Double> tempSpinner;
    @FXML
    VBox vbox_droite;
    @FXML
    TextField nameField;
    @FXML
    TextField weightField;
    @FXML
    TextField tempField;
    @FXML
    TreeView<Sensor> tree;

    public WeatherController(List<Sensor> sensorsList, String url, String title) throws IOException {
        super(title, url);

        sensorList = sensorsList;
        TreeItem<Sensor> root = new TreeItem<>();
        root.setExpanded(true);
        tree.setShowRoot(false);
        tree.setRoot(root);
        TreeItemFactoryVisitor treeFactory = new TreeItemFactoryVisitor();

        for(Sensor s: sensorList){
            if(s instanceof VirtualSensor){
                VirtualSensor virtualSensor = (VirtualSensor) s;
                TreeItem<Sensor> item = virtualSensor.accept(treeFactory);
                root.getChildren().add(item);
            }
            else{
                TreeItem<Sensor> item = s.accept(treeFactory);
                root.getChildren().add(item);
            }
        }
        tree.setVisible(true);
        vbox_droite.setVisible(false);

        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        tempCol.setCellValueFactory(new PropertyValueFactory<>("temp"));



        this.treeViewBinding();
    }

    public void treeViewBinding() {
        //------------------------------------------------------------------------------------------------------
        tree.setCellFactory(param -> new TreeCell<Sensor>() {
            @Override
            protected void updateItem(Sensor sensor, boolean empty) {
                super.updateItem(sensor, empty);
                if (empty) {
                    textProperty().unbind();
                    setText("");
                } else {
                    textProperty().bind(sensor.getName());
                    setOnMouseClicked(event -> selectItem());
                }

            }
        });
        //------------------------------------------------------------------------------------------------------
        // SPINNER
        tempSpinner.setVisible(true);

        Sensor sensor = sensorList.get(0);
        sensor.getTempProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("La température du capteur "+sensor.getName().getValue()+ " a changée de "+oldValue + " à " + newValue);
        });

        SpinnerValueFactory<Double> spinnerValueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(-273, 10000, sensor.getTemp(), 0.5);
        tempSpinner.setValueFactory(spinnerValueFactory);
        tempSpinner.getValueFactory().valueProperty().bindBidirectional(sensor.getTempProperty().asObject());
        sensor.updateTemperature();
      //  System.out.println("initialize spinner");
      //  System.out.println(tempSpinner.getValueFactory().getValue());

    }

    public void selectItem(){
        TreeItem<Sensor> selectedItem = tree.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            vbox_droite.setVisible(true);
            if(selectedSensor != selectedItem){
                if(selectedSensor != null ){
                    vboxUnbind(selectedSensor.getValue());
                }

                vboxBinding(selectedItem.getValue());
                selectedSensor = selectedItem;
            }
        }
    }


    public void vboxUnbind(Sensor s){
        nameField.textProperty().unbindBidirectional(s);
        weightField.textProperty().unbindBidirectional(s);
        tableview.getItems().clear();
        tableview.refresh();

    }
    public void vboxBinding(Sensor s){
        nameField.textProperty().bindBidirectional(s.getName());
        weightField.textProperty().bindBidirectional(s.getWeight(), new NumberStringConverter());
        tempField.textProperty().bindBidirectional(s.getTempProperty(), new NumberStringConverter());

        if(s instanceof VirtualSensor){
            VirtualSensor s2 = (VirtualSensor) s;
            ObservableList<Sensor> o = FXCollections.observableList(s2.getSensors());
            tableview.setItems(o);
            tableview.getColumns().addAll(nameCol, tempCol);
        }

    }
}
