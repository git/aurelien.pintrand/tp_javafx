package views;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class FXMLWindow extends Stage {

    public FXMLWindow(String title, String url) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(url));
        fxmlLoader.setController(this);
        Parent group = fxmlLoader.load();
        Scene scene = new Scene(group);
        this.setScene(scene);
        this.show();
    }
}
