package modele;

import javafx.beans.property.*;

public abstract class Sensor {
    int id;
    private TempGenerator tempGenerator;
    protected StringProperty name;
    protected DoubleProperty weight;
    protected DoubleProperty temp = new SimpleDoubleProperty();

    public void setTemp(double tmp){
        this.temp.setValue(tmp);
    }

    public double getTemp() {
        return temp.getValue();
    }
    public StringProperty getName(){
        return name;
    }

    public DoubleProperty getTempProperty(){
        return temp;
    }

    public abstract void updateTemperature();

    public abstract <T> T accept(Visitor<T> v);

    public DoubleProperty getWeight(){
        return weight;
    }

    public TempGenerator getTempGenerator() {
        return tempGenerator;
    }

    public void setTempGenerator(TempGenerator tempGenerator) {
        this.tempGenerator = tempGenerator;
    }

}
