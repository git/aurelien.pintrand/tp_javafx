package modele;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import static java.lang.System.exit;

public class ConcreteSensor extends Sensor {
    public ConcreteSensor(int id, String name, int weight, String generation) {
        this.name = new SimpleStringProperty(name);
        this.id = id;
        this.weight = new SimpleDoubleProperty(weight);
        this.changeGenerationMethod(generation);
        updateTemperature();
        // System.out.println("La température du capteur "+this.getName().getValue()+" est de "+this.getTemp());
    }

    public void changeGenerationMethod(String generation) {
        switch(generation){
            case "random":
                this.setTempGenerator(new TempRandom());
                break;
            case "cpu":
                this.setTempGenerator(new TempCPU());
                break;
            default:
                System.out.println("Choix de génération incorrect.");
                exit(1);
        }
    }

    @Override
    public void updateTemperature() {
        this.setTemp(getTempGenerator().generateTemperature());
        System.out.println("température de "+this.getName().getValue()+": "+getTemp());
    }

    @Override
    public <T> T accept(Visitor<T> v) {
        return v.visit(this);
    }
}

