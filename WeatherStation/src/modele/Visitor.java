package modele;

public interface Visitor<T> {
    T visit(ConcreteSensor c);
    T visit(VirtualSensor v);
}
