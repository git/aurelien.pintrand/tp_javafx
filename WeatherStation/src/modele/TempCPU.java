package modele;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TempCPU implements TempGenerator, Runnable {
    private Thread thread;
    private boolean running = false;
    double actualtemp = 666;
    public void start() {
        running = true;
        thread.start();
    }

    public void stop() {
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Override
    public double generateTemperature() {
        start();
        return actualtemp;
    }

    @Override
    public void run() {
        while (running) {
            try {
                String cmd = "wmic /namespace:\\\\root\\wmi PATH MSAcpi_ThermalZoneTemperature get CurrentTemperature";
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    if (!line.contains("CurrentTemperature")) {
                        int temp = Integer.parseInt(line);
                        temp =temp / 10 - 273;
                        System.out.println("Current CPU temperature: " + temp + " C");
                        actualtemp = temp;
                    }
                }
                reader.close();
                // Sleep for a certain amount of time before getting the temperature again
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
