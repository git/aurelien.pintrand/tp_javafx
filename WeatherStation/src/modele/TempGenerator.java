package modele;

import java.util.List;

public interface TempGenerator {
    public double generateTemperature();
}
