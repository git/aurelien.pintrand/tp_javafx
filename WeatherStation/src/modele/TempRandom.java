package modele;

public class TempRandom implements TempGenerator{
    @Override
    public double generateTemperature() {
        double min = -273.15;
        double max = 1000;
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
}
