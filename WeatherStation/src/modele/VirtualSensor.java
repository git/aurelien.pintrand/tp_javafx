package modele;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VirtualSensor extends Sensor {
    private List<Sensor> sensors  =new ArrayList<>();
    private Map<Sensor, Double> weights = new HashMap<>();

    public VirtualSensor(List<Sensor> sensorsList, int id, String name, int weight) {
        this.name = new SimpleStringProperty(name);
        this.id = id;
        this.weight = new SimpleDoubleProperty(weight);
        this.temp = new SimpleDoubleProperty(0);
        this.updateTemperature();
      //  System.out.println("La température du capteur "+this.getName().getValue()+" est de "+this.getTemp());
    }

    public void addSensor(Sensor sensor, double weight) {
        sensors.add(sensor);
        weights.put(sensor, weight);
        sensor.getTempProperty().addListener((observable, oldValue, newValue) -> updateTemperature());
        updateTemperature();
    }

    @Override
    public void updateTemperature() {
        double tempSum = 0;
        double weightSum = 0;
        if(sensors.isEmpty()){
            this.setTemp(0);
        }
        else {
            for (Sensor sensor : sensors) {
                tempSum += sensor.getTemp();
                weightSum += weights.get(sensor);
                System.out.println("capteur " + sensor.getName().getValue() + " tempSum: " + tempSum + " weightSum: " + weightSum);
            }
            setTemp(tempSum / sensors.size());
            System.out.println("température finale de "+this.getName().getValue()+" : "+this.getTemp());
        }
    }

    @Override
    public <T> T accept(Visitor<T> v) {
        return v.visit(this);
    }

    public List<Sensor> getSensors(){
        return sensors;
    }
}

