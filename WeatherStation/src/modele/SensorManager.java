package modele;

import java.util.ArrayList;
import java.util.List;

public class SensorManager {
    private static SensorManager instance;

    private List<Sensor> sensorList;

    private SensorManager() {
        sensorList = new ArrayList<>();
    }

    public static SensorManager getInstance() {
        if (instance == null) {
            instance = new SensorManager();
        }
        return instance;
    }

    public void addSensor(Sensor sensor) {
        sensorList.add(sensor);
    }

    public List<Sensor> getSensorList() {
        return sensorList;
    }
}

