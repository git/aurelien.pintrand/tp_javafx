package modele;

import javafx.scene.control.TreeItem;

public class TreeItemFactoryVisitor implements Visitor<TreeItem<Sensor>>{
    @Override
    public TreeItem<Sensor> visit(ConcreteSensor c) {
        return new TreeItem<>(c);
    }

    @Override
    public TreeItem<Sensor> visit(VirtualSensor v) {
        TreeItem<Sensor> item = new TreeItem<>(v);
        for (Sensor sensor: v.getSensors()) {
            TreeItem<Sensor> childItem;
            if(sensor instanceof VirtualSensor){
                childItem = visit((VirtualSensor) sensor);
            }
            else {
                childItem = new TreeItem<>(sensor);
            }
            item.getChildren().add(childItem);
        }
        return item;
    }
}
