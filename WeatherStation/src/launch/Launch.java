package launch;

import javafx.application.Application;
import javafx.stage.Stage;
import modele.ConcreteSensor;
import modele.SensorManager;
import modele.VirtualSensor;
import views.WeatherController;

import java.util.ArrayList;

public class Launch extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        //CREATE SENSOR MANAGER
        SensorManager.getInstance();

        //CREATE ALL SENSORS
        //CREATE VIRTUAL SENSOR 2 & CHILDREN
        ConcreteSensor concreteSensor3 = new ConcreteSensor(4, "concrete3",78,"random");
        ConcreteSensor concreteSensor4 = new ConcreteSensor(5,"concrete4",47,"random");
        VirtualSensor virtualSensor2 = new VirtualSensor(new ArrayList<>(), 4, "Virtual Sensor 2", 69);
        virtualSensor2.addSensor(concreteSensor3, concreteSensor3.getWeight().getValue());
        virtualSensor2.addSensor(concreteSensor4, concreteSensor4.getWeight().getValue());

        //CREATE VIRTUAL SENSOR 1 & CHILDREN
        ConcreteSensor concreteSensor1 = new ConcreteSensor(2, "concrete1",78,"random");
        ConcreteSensor concreteSensor2 = new ConcreteSensor(3,"concrete2",47,"random");
        VirtualSensor virtualSensor1 = new VirtualSensor(new ArrayList<>(), 1, "Virtual Sensor 1", 54);
        virtualSensor1.addSensor(concreteSensor1, concreteSensor1.getWeight().getValue());
        virtualSensor1.addSensor(concreteSensor2, concreteSensor2.getWeight().getValue());

        virtualSensor1.addSensor(virtualSensor2, virtualSensor2.getWeight().getValue());
        //ADD SENSORS TO MANAGER
        SensorManager.getInstance().addSensor(virtualSensor1);
        //SensorManager.getInstance().addSensor(virtualSensor2);

        //MARCHE PAS ( C SAD )
       // concreteSensor1.changeGenerationMethod("cpu");

        WeatherController weatherController = new WeatherController(SensorManager.getInstance().getSensorList(), "FXML/WeatherStation.fxml","WeatherStation");

    }
}
