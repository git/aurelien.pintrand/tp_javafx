## Ce qui marche:
- implémentation du patron composite ( chaque capteur peut être composé d'un capteur virtuel ou concret)
- implémentation du patron de stratégie ( possibilité d'avoir plusieurs types de génération )
- Treeview composé de tous les capteurs
- détails sur le capteur affichés sur la droite (avec une tableview qui affiche les capteurs internes si c'est un capteur virtuel)

## Ce qui ne marche pas:
- Bug de treeview : unbind qui ne marche pas lors de la sélection d'un autre capteur
- La tableView s'efface lors de la sélection d'un autre capteur mais ne réaffiche pas les valeurs
- La génération par CPU ne marche pas.
- Design pas dingue
- Pas d'images pour les capteurs 